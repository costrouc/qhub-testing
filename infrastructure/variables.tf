variable "name" {
  type    = string
  default = "asdfqwer"
}

variable "environment" {
  type    = string
  default = "dev"
}

# jupyterhub
variable "endpoint" {
  description = "Jupyterhub endpoint"
  type        = string
  default     = "asdf.qhub.dev"
}

variable "jupyterhub-image" {
  description = "Jupyterhub user image"
  type = object({
    name = string
    tag  = string
  })
  default = {
    name = "quansight/qhub-jupyterhub"
    tag  = "d52cea07f70cc8b35c29b327bbd2682f29d576ad"
  }
}

variable "jupyterlab-image" {
  description = "Jupyterlab user image"
  type = object({
    name = string
    tag  = string
  })
  default = {
    name = "quansight/qhub-jupyterlab"
    tag  = "d52cea07f70cc8b35c29b327bbd2682f29d576ad"
  }
}

variable "dask-worker-image" {
  description = "Dask worker image"
  type = object({
    name = string
    tag  = string
  })
  default = {
    name = "quansight/qhub-dask-worker"
    tag  = "d52cea07f70cc8b35c29b327bbd2682f29d576ad"
  }
}


